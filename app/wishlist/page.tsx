"use client";

import BookList from "@/components/BookList";
import { useWishlistStore } from "@/lib/useWishlist";

export default function WishList() {
  const wishlists = useWishlistStore((state) => state.wishlists);

  return (
    <div className="flex flex-col items-center justify-center h-auto my-6">
      <div className="container mx-auto">
        <BookList books={wishlists} />
      </div>
    </div>
  );
}
