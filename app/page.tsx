"use client";

import BookList from "@/components/BookList";
import SearchBar from "@/components/SearchBar";
import { useSearchParams } from "next/navigation";
import { useEffect, useState } from "react";

export default function Home() {
  const [books, setBooks] = useState<any[]>([]);
  const searchParams = useSearchParams();
  const keyword = searchParams.get("keyword");

  useEffect(() => {
    async function getBooks(input_keyword: string) {
      const res = await fetch(
        `https://www.googleapis.com/books/v1/volumes?q=${input_keyword}`
      );

      const data = await res.json();
      setBooks(data.items);
    }

    if (keyword) {
      setBooks([]);
      getBooks(keyword);
    }
  }, [keyword, setBooks]);

  return (
    <div className="flex flex-col items-center justify-center h-auto my-6">
      <div className="container mx-auto">
        <SearchBar />
        <div className="mb-4"></div>
        <BookList books={books} />
      </div>
    </div>
  );
}
