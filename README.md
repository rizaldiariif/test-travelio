## Penjelasan Singkat

Ini adalah Aplikasi Test Mini Project dari Travelio.
Demo dari aplikasi dapat diakses di sini [Demo](https://test-travelio-rizaldiariif.vercel.app/)

### Features

Fitur-Fitur yang ada dalam aplikasi ini adalah:

- [x] Pencarian data buku dari [API Google Books Volume](https://www.googleapis.com/books/v1/volumes?q=keyword)
- [x] Dari data API menampilkan Judul, Author/Publisher, Rating, dan Thumbnail
- [x] Menyimpan buku ke dalam wishlist (penyimpanan wishlist berada di LocalStorage pada browser)
- [x] Mobile Friendly sehingga dapat digunakan dalam perangkat desktop maupun mobile
- [ ] Halaman detail buku

### Tech Stack

1. NextJS
   Menggunakan NextJS agar mempermudah _reactivity_ dari bawaan library ReactJS, fitur App Routing dari NextJS yang mempermudah pengelompokan _routing_.
2. TailwindCSS
   Menggunakan TailwindCSS untuk mempercepat _styling_ dari masing-masing komponen dan halaman.
3. Browser LocalStorage
   Menggunakan Browser LocalStorage untuk menyimpan data buku yang di-_wishlist_ oleh user.

### How To Run in Your Machine

1. Install Dependencies

```bash
npm install
# or
yarn install
# or
pnpm install
```

2. Menggunakan Local Node Server

```bash
npm  run  dev
# or
yarn  dev
# or
pnpm  dev
```

3. Menggunakan Docker Container

```bash
# build the image
docker-compose build

# start the docker image
docker-compose up
# or start the docker image in background mode
docker-compose up -d
```

4. Buka [http://localhost:3000](http://localhost:3000) dalam browser untuk membuka aplikasinya.
