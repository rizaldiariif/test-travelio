import { create } from "zustand";

interface WishlistState {
  wishlists: any[];
  addWishlist: (book: any) => void;
  removeWishlist: (id: string) => void;
  initiateWishlist: (books: any[]) => void;
}

export const useWishlistStore = create<WishlistState>((set) => ({
  wishlists: [],
  addWishlist: (book: any) =>
    set((state: WishlistState) => {
      const new_wishlists = Array.from(state.wishlists);
      new_wishlists.push(book);
      syncLocalStorage(new_wishlists);
      return { wishlists: new_wishlists };
    }),
  removeWishlist: (id: string) =>
    set((state: WishlistState) => {
      let new_wishlists = Array.from(state.wishlists);
      new_wishlists = new_wishlists.filter((book) => book.id != id);
      syncLocalStorage(new_wishlists);
      return { wishlists: new_wishlists };
    }),
  initiateWishlist: (books: any[]) =>
    set((_state: WishlistState) => ({ wishlists: books })),
}));

const syncLocalStorage = (wishlist: any[]) => {
  localStorage.setItem("wishlist", JSON.stringify(wishlist));
};
