function findSecondLargest(inputs) {
  // Cek apakah panjang input < 2
  if (inputs.length < 2) {
    throw new Error("Ukuran Array kurang dari 2");
  }

  // Inisiasi 2 angka pertama
  let firstLargest = inputs[0];
  let secondLargest = inputs[1];

  // Dari 2 angka pertama, cek mana yang lebih besar
  if (secondLargest > firstLargest) {
    [firstLargest, secondLargest] = [secondLargest, firstLargest];
  }

  // Pengecekan dari index 2 dst
  for (let i = 2; i < inputs.length; i++) {
    const currentNumber = inputs[i];

    if (currentNumber > firstLargest) {
      // Apabila paling besar, geser index 1 ke 2, lalu nilai baru ke index 1. 
      secondLargest = firstLargest;
      firstLargest = currentNumber;
    } else if (currentNumber > secondLargest && currentNumber !== firstLargest) {
      // Apabila nilai angka baru lebih besar dari index 2 dan nilainya tidak sama dengan angka index 1. jadikan nilai baru ke index 2 
      secondLargest = currentNumber;
    }
  }

  return secondLargest;
}

// Inisiasi fungsi komparasi value a dan b
function compareCriteria(a, b) {
  // Lakukan grading nilai terhadap Grade yang sudah ditentukan
  const gradeOrder = { 'A': 1, 'B': 2, 'C': 3, 'D': 4 };

  // Ekstrak data a dan b berdasarkan splitternya yaitu tanda "|"
  const [_nameA, pointsA, gradeA] = a.split('|');
  const [_nameB, pointsB, gradeB] = b.split('|');

  if (gradeOrder[gradeA] !== gradeOrder[gradeB]) {
    /** 
    *   Lakukan cek terhadap grade dahulu, apabila grade berbeda
    *   Kurangkan grade dari value a dengan b
    *   Apabila hasilnya positif (> 0) berarti yang terpilih adalah value a
    *   Apabila hasilnya negatif (<= 0) berarti yang terpilih adalah value b
    */
    return gradeOrder[gradeA] - gradeOrder[gradeB];
  } else {
    /**
    *   Apabila grade sama, bandingkan point nya
    *   Kurangkan point dari value a dengan b
    *   Apabila hasilnya positif (> 0) berarti yang terpilih adalah value a
    *   Apabila hasilnya negatif (<= 0) berarti yang terpilih adalah value b
    */
    return pointsB - pointsA;
  }
}

function sortPeopleByGradeAndPoint(data) {
  // Panggil fungsi sort bawaan dari javascript dengan criteria yang sudah kita buat di fungsi sebelumnya
  data.sort(compareCriteria);

  // Hasil data yang sudah disorting, lakukan ekstraksi data dengan splitter "|" lalu ambil namanya saja yaitu di index 0
  const result = data.map(item => item.split('|')[0]);
  return result;
}

function isPalindrome(input_string) {
  // Inisiasi value lebar huruf sebagai acuan perbandingan huruf
  const length = input_string.length;

  // Lakukan iterasi sampai setengah dari jumlah kata saja
  for (let i = 0; i < length / 2; i++) {
    /**
     * Lakukan pengecekan index yang sedang diiterasi dan kebalikannya [length - 1 - index]
     * Example:
     * "racecar"
     * iterasi 1
     * index 0 dibandingkan dengan index [length - 1 - i]->[7-1-0]->6
     * index 0 dibandingkan dengan index 6
     * r dibandingkan dengan r
     * hasilnya true
     * iterasi 2
     * index 1 dibandingkan dengan index [length - 1 - i]->[7-1-1]->5
     * index 1 dibandingkan dengan index 5
     * a dibandingkan dengan a
     * hasilnya true
     * ...dst
     */
    if (input_string[i] !== input_string[length - 1 - i]) {
      return false;
    }
  }
  return true;
}