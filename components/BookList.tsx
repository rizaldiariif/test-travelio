"use client";

import Image from "next/image";
import Link from "next/link";
import Rater from "react-rater";
import { useWishlistStore } from "@/lib/useWishlist";

type Props = { books: any[] };

const BookList = ({ books }: Props) => {
  const wishlists = useWishlistStore((state) => state.wishlists);
  const addWishlist = useWishlistStore((state) => state.addWishlist);
  const removeWishlist = useWishlistStore((state) => state.removeWishlist);

  if (!books || books.length == 0) {
    return (
      <div className="relative">
        <div className="grid w-full grid-cols-2 gap-4 mb-8 opacity-50 lg:grid-cols-6">
          {Array(12)
            .fill(null)
            .map((_, index) => (
              <div
                className="relative block col-span-1 overflow-hidden border rounded-lg shadow-md h-80 bg-slate-400"
                key={index}
              ></div>
            ))}
        </div>
        <p className="absolute w-full text-xl font-semibold text-center transform -translate-x-1/2 lg:text-6xl lg:-translate-y-1/2 lg:w-1/2 lg:top-1/2 top-20 left-1/2 text-slate-800">
          Please Search for Books with Another Keywords
        </p>
      </div>
    );
  }

  return (
    <>
      <div className="grid w-full grid-cols-2 gap-4 mb-8 lg:grid-cols-6">
        {books.map((book: any) => (
          <div
            className="relative block col-span-1 overflow-hidden border rounded-lg shadow-md"
            key={book.id}
          >
            <div className="relative flex flex-col items-start justify-center w-full h-auto gap-2 text-start">
              <div className="relative w-full h-auto aspect-3/4">
                <Image
                  src={
                    book.volumeInfo.imageLinks
                      ? book.volumeInfo.imageLinks.thumbnail
                      : "/default-thumbnail.png"
                  }
                  fill={true}
                  layout="fill"
                  alt={`Thumbnail of ${book.etag}`}
                  className="object-cover"
                />
              </div>
              <div className="w-full p-2 mb-8">
                <h2 className="text-md line-clamp-2">
                  {book.volumeInfo.title}
                </h2>
                <h3 className="text-sm line-clamp-1">
                  {book.volumeInfo.authors
                    ? book.volumeInfo.authors.join(", ")
                    : book.volumeInfo.publisher}
                </h3>
                <Rater
                  rating={book.volumeInfo.averageRating}
                  interactive={false}
                />
                <div className="flex items-center gap-2 mt-2">
                  {/* <Link
                    href={`/volume/${book.id}`}
                    className="flex-grow border"
                  >
                    Detail
                  </Link> */}
                  {wishlists.find((item) => item.id == book.id) ? (
                    <button
                      onClick={removeWishlist.bind(this, book.id)}
                      className="flex items-center justify-start flex-grow text-center"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        fill="currentColor"
                        className="w-6 h-6 text-red-500"
                      >
                        <path d="M11.645 20.91l-.007-.003-.022-.012a15.247 15.247 0 01-.383-.218 25.18 25.18 0 01-4.244-3.17C4.688 15.36 2.25 12.174 2.25 8.25 2.25 5.322 4.714 3 7.688 3A5.5 5.5 0 0112 5.052 5.5 5.5 0 0116.313 3c2.973 0 5.437 2.322 5.437 5.25 0 3.925-2.438 7.111-4.739 9.256a25.175 25.175 0 01-4.244 3.17 15.247 15.247 0 01-.383.219l-.022.012-.007.004-.003.001a.752.752 0 01-.704 0l-.003-.001z" />
                      </svg>
                    </button>
                  ) : (
                    <button
                      onClick={addWishlist.bind(this, book)}
                      className="flex items-center justify-start flex-grow text-center"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-6 h-6"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
                        />
                      </svg>
                    </button>
                  )}
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default BookList;
