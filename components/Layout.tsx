import Link from "next/link";
import WishlistInitializer from "./WishlistInitializer";

type Props = { children: any };

const Layout = (props: Props) => {
  return (
    <>
      <WishlistInitializer />
      <header className="w-full h-20 bg-black">
        <div className="container flex items-center justify-between h-full mx-auto">
          <Link href="/" className="text-xl font-black text-white">
            BOOKS
          </Link>

          <Link href="/wishlist" className="text-xl font-black text-white">
            MY WISHLIST
          </Link>
        </div>
      </header>
      {props.children}
      <footer className="flex items-center justify-center w-full h-20 text-center bg-black">
        <p className="text-sm text-white">
          © Copyright 2023. Test Travelio. All rights reserved.
        </p>
      </footer>
    </>
  );
};

export default Layout;
