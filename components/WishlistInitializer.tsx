"use client";

import { useWishlistStore } from "@/lib/useWishlist";
import { useEffect } from "react";

type Props = {};

const WishlistInitializer = (props: Props) => {
  const initiateWishlist = useWishlistStore((state) => state.initiateWishlist);

  useEffect(() => {
    const local_wishlist = localStorage.getItem("wishlist") ?? "[]";
    initiateWishlist(JSON.parse(local_wishlist));
  }, [initiateWishlist]);

  return null;
};

export default WishlistInitializer;
