"use client";

import { useSearchParams } from "next/navigation";
import { useRouter } from "next/navigation";
import { FormEvent } from "react";

type Props = {};

const SearchBar = (props: Props) => {
  const searchParams = useSearchParams();
  const router = useRouter();

  const onSubmitKeyword = (e: FormEvent<HTMLFormElement>) => {
    const data = new FormData(e.currentTarget);
    const keyword = data.get("keyword") as string;

    router.push(`?keyword=${keyword}`);
    e.preventDefault();
  };

  return (
    <form
      onSubmit={onSubmitKeyword}
      className="flex items-center justify-center w-auto h-10 gap-4 mx-auto lg:w-1/4"
    >
      <input
        type="text"
        name="keyword"
        defaultValue={searchParams.get("keyword") ?? ""}
        className="flex-grow block h-full px-2 border"
        placeholder="Cari buku di sini"
      />
      <button type="submit" className="block h-full px-6 py-2 border">
        Search
      </button>
    </form>
  );
};

export default SearchBar;
